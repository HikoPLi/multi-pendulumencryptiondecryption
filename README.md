# Pendulum Model Encryption
This is a Python implementation of an encryption algorithm based on a pendulum model. The algorithm encrypts messages by mapping each character to a set of coordinates on a 2D plane, using the positions of oscillating nodes as angles for the mapping.

# Output
![Terminal-Output](https://gitlab.com/HikoPLi/multi-pendulumencryptiondecryption/-/raw/main/result/outputs.png)
![JSON-Output](https://gitlab.com/HikoPLi/multi-pendulumencryptiondecryption/-/raw/main/result/jsonOutputs.png)

# Clone 
Run the following command in the terminal:

    git clone https://gitlab.com/HikoPLi/multi-pendulumencryptiondecryption.git
    cd multi-pendulumencryptiondecryption

After doing this, you can run this command in the terminal to run the Python code:

    python main.py
    
# Usage
To use this algorithm, import it into your Python code:  

    from pendulum_model_encryption import encrypt

Then, call the encrypt function to encrypt a message:  

    message = "Hello, world!"
    length = 10
    nodes = 5

    points = encrypt(message, length, nodes)

The encrypt function takes three arguments: the message to be encrypted, the desired length of the 2D plane, and the number of oscillating nodes to use when mapping the message to coordinates. It returns a list of (x, y) coordinates representing the encrypted message.  

# Saving Encrypted Messages
To save an encrypted message as a JSON file, you can use the saveEncode function:  

    from pendulum_model_encryption import saveEncode

    message = "Hello, world!"
    length = 10
    nodes = 5

    points = encrypt(message, length, nodes)

    saveEncode(message, points)

This will create a file named "Message-points.json" in the current directory, containing a JSON-encoded list of dictionaries, each representing an encrypted message and its corresponding set of coordinates.  

# License
This implementation of the pendulum model encryption algorithm is provided under the MIT License. Please see the LICENSE file for details.  
[MIT-License](https://gitlab.com/HikoPLi/multi-pendulumencryptiondecryption/-/blob/main/README.md)

# Contact
If you have any questions or suggestions, please feel free to contact the author at lihikopgithub@outlook.com.  
  
I hope this explanation helps you. If you have any further questions or requests, please don't hesitate to let me know.

# Buy Me A Coffee
Begging for a coffee(Alipay, WeChat Pay, PayPal)  
[Click-Here](https://gitlab.com/HikoPLi/multi-pendulumencryptiondecryption/-/blob/main/buymecoffee.jpg)
