from dotenv import load_dotenv
import math
import json


def saveEncode(message, points):
    with open("Message-points.json", "r") as file:
        data = json.load(file)

    data.append({"message": message, "points": points})

    with open("Message-points.json", "w") as file:
        json.dump(data, file, indent=2)


def encrypt(message, length, nodes):
    message = message  # .lower().replace(" ", "").replace(".", "").replace(",", "")
    # 计算每个摆动节点之间的间隔
    interval = length / (nodes - 1)
    # 初始化每个摆动节点的角度为0
    angles = [0] * nodes
    # 将每个字母映射到对应的坐标点
    points = []
    for letter in message:
        # 根据字母的ASCII码值计算出摆动节点的索引
        index = (ord(letter) - 97) % nodes
        # 计算摆动节点的角度
        angles[index] += math.pi / 2
        # 计算摆动节点的坐标点
        x = interval * index + math.sin(angles[index]) * length / 2
        y = math.cos(angles[index]) * length / 2
        points.append((x, y))
    saveEncode(message, str(points).replace("[", "").replace("]", ""))
    return points


def main():
    message = input("Enter what you need to encode: ")
    length = 2  # 将长度设为10
    nodes = 3  # 将摆动节点个数设为5
    points = encrypt(message, length, nodes)
    print(f"Encode(With replace): {points}")
    print(
        "Endcode: "
        + str(points)
        .replace("(", "")
        .replace(")", "")
        .replace(".", "")
        .replace(" ", "")
        .replace(",", "")
        .replace("[", "")
        .replace("]", "")
        .replace("-", "")
    )


if __name__ == "__main__":
    load_dotenv()
    main()
